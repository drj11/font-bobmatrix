# Bob Matrix

[Bob Matrix Mono](bob.ttf) is a font-mashup:
The dot matrix font from the Epson FX-80 is re-used as a modular design, 
with the “dot” being replaced with a tiny image of J. R. "Bob" Dobbs;
the image itself coming from the Atari ST pixel font.

We recommend using Bob Matrix at a size of 192 pixels.

![All characters in Bob Matrix Mono](plaque-BobMatrixMono-192.png)


## Technical Notes

A slashed zero is available in the Private Use Area at U+E030.
This will become available as a selectable alternate to /zero in
some future release.


## Experiment and Evaluation

This font is experimental in various ways,
in particular in its construction.
It is built using the `font8` tools and Python scripts of
varying robustness.
This has driven various changes in the tools and
brought to light quite a few bugs.
Please be especially vigilant in seeking and reporting bugs.


## Vector Bob

The image of Bob has been extracted from a `PNG` file as a
vector, using `vec8`.

The vector drawing was reposition to be centred at [0 0];
translating by [-8 -8] using https://yqnn.github.io/svg-path-editor/#
then pasted back here.

M -3 -8 L -3 -7 L -4 -7 L -4 -6 L -5 -6 L -5 1 L -4 1 L -4 3 L -3 3 L -3 5 L -4 5 L -4 6 L -5 6 L -5 5 L -6 5 L -6 8 L -3 8 L -3 6 L -1 6 L -1 7 L 3 7 L 3 6 L 4 6 L 4 4 L 5 4 L 5 1 L 6 1 L 6 -6 L 5 -6 L 5 -7 L 4 -7 L 4 -8 Z M 3 -5 L 3 -6 L 4 -6 L 4 -5 L 5 -5 L 5 -2 L 2 -2 L 2 -1 L 1 -1 L 1 0 L 2 0 L 2 -1 L 3 -1 L 3 0 L 4 0 L 4 -1 L 5 -1 L 5 1 L 4 1 L 4 2 L 3 2 L 3 1 L 2 1 L 2 2 L 0 2 L 0 -1 L -1 -1 L -1 -2 L -4 -2 L -4 -3 L -3 -3 L -3 -5 Z M -4 0 L -4 -1 L -3 -1 L -3 0 L -1 0 L -1 1 L -2 1 L -2 2 L -3 2 L -3 1 L -4 1 Z M -2 3 L -2 2 L -1 2 L -1 3 Z M 2 3 L 2 2 L 3 2 L 3 3 Z M -1 4 L -1 3 L 2 3 L 2 4 Z M 3 4 L 3 3 L 4 3 L 4 4 Z M 2 5 L 2 4 L 3 4 L 3 6 L -1 6 L -1 5 Z

## SOURCES

vendor/atari-st.ttf
from https://fontstruct.com/fontstructions/show/1687728/atari-st

# END
