#!/bin/sh

set -e

hb-view --background=000000 --foreground=ffffff --margin=0 --font-size=8 --unicodes 250c,2510,000a,2514,2518 vendor/atari-st.ttf > bob.png

vec8 bob.png

## somehow process vector

(cd ../dotter
PYTHONPATH=$PWD python bin/makefontsource.py --name Bob --dot bob --radius 0.5 --hpitch 3.75
)

rm -fr Bob-src
cp -pr ../dotter/Bob-src Bob-src/

# Check name of generated CSV file and append our own control file
ls Bob-src/control-dotter.csv > /dev/null
cat control-bob.csv >> Bob-src/control-dotter.csv

ttf8 -dir bob -control Bob-src/control-*.csv Bob-src/*.svg
f8name -dir bob -control control-bob.csv
assfont -dir bob
glys bob.ttf
plak --font-size=192 $(cat colour.txt) --order code bob.ttf > plaque-BobMatrixMono-192.png
plak --font-size=96 $(cat colour.txt) --order code bob.ttf > plaque-BobMatrixMono-96.png

kitty icat plaque-BobMatrixMono-192.png
kitty icat plaque-BobMatrixMono-96.png
