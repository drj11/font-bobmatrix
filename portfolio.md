# "Bob" Matrix Mono Portfolio

<figure>
<img alt="J. R. &quot;Bob&quot; Dobbs shown with his familiar
smile and pipe in a newsprint style black-and-white image"
src="image/ClipBob.png" />
<figcaption>© The SubGenius Foundation, Inc. for J. R. "Bob"
Dobbs</figcaption>
</figure>

When i was a teenager i had an Atari ST and i also printed out a
lot of programming documentation on my precious
dot matrix printer.
Fast forward to 2023 and i came across a
[hobbyist “emulator” of the classic dot matrix printer the Epson
FX-80](https://github.com/MurphyMc/EPHEX-80), that 
was written so the output of greeting card software running on
an emulated Apple II could be converted to SVG.
I took this opportunity to make a font from the emulated FX-80
output.

The making of the font is of course scripted, in Python.
Having a script makes it possible to vary some features of the
font such as dot size and shape, overall pitch, and dot
position;
at least in principle.
It makes many experiments [a small matter
of
programming](https://mitpress.mit.edu/9780262140539/a-small-matter-of-programming/).

This experiment proposes to replace the dot with a drawing of
[J. R. "Bob"
Dobbs](https://en.wikipedia.org/wiki/J._R._%22Bob%22_Dobbs).
Specifically the J. R. "Bob" Dobbs pixel graphic found, split
into 4 pieces, in the ROM of the
[Atari ST character set](https://en.wikipedia.org/wiki/Atari_ST_character_set).

Here is J. R. "Bob" Dobbs from the Atari ST ROM font:

<img alt="J. R. &quot;Bob&quot; replicated in pixels from the Atari ST ROM font"
src="image/bob.png" />

The "Bob" in J. R. "Bob" Dobbs should always have quotes around
the "Bob" when using correct orthography (and, yes, the
symmetric anti-typographical ASCII quotes).
Hence, the _true name_ of the font should be **"Bob" Matrix Mono**,
but it’s too experimental to put quotes in the name of a font, so
it tends to be called **Bob Matrix Mono**.


## The Small Matter of Programming

- the found code had to be converted to Python 3;
- new scripts written that “drive” the Epson FX-80 emulator to
  produce an SVG file containing the dot matrix font;
- my existing Font 8 `ttf8` program could convert SVG to TTF,
  but it had to be extended to convert SVG `<use>` elements to 
  TrueType components in the `glyf` table;
- various edits and extensions to implement switchable dot
  shape.

There have been a few non-programming tweaks too:

- **g** and other glyphs that descended were off-by-one
  vertically in the found bitmaps; the found code embodied a
  misunderstanding of how the Epson FX-80 encoding works that i had to
  correct.
- The numeric codes for the Epson FX-80 characters are ASCII
  with proprietary additions; they have all been assigned their
  correct Unicode code points.
- I drew a **š**, hinting at making more Latin glyphs available
  before a proper publication.
- The slashed zero, available on the Epson FX-80 via a DIP
  switch setting, has been assigned to the Private Use Area.

## Showing

Here's the full repertoire.
Though this image is pixel-perfect, meaning each "Bob" is 16
pixels high and pixels are only black or white, it may not
be possible to view it as such. 
For me in Firefox, the browsers insists on a smoothing filter,
regardless of how i zoom.
Downloading the image and opening it in Apple Preview does
show it pixel-perfect at 100% zoom.

![a showing of the Bob Matrix Mono font. Each glyph is made up
of little copies of a pixelated "Bob" arranged like dots in a
dot matrix printout](plaque-BobMatrixMono-192.png)

Note the little glitchy "Bob" at the bottom right,
which is not pixel perfect.
This is an artefact of the way in which
the prototype "Bob" is stored in the font file as a component.
"Bob" is scaled exactly before use in all the other glyphs.
Ordinarily font glyph components are not visible in a font
because they do have have a codepoint,
but `ttf8` puts glyphs without an ordinary codepoint
in the Private Use Area, which can be useful for
debugging, but i’m not sure is useful for production.

There is some irony in that
to see J. R. "Bob" Dobbs in all his pixelated glory, its
best to zoom in on a vector representation;
in this case, a single **B**.

![B](image/B.svg)


# END
